<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>greensky-STC-Assignment-Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    <link rel="stylesheet" href="style.css">
	<style>
.col-md-6.col-49 {
    width: 49%;
    margin: 10px 0px;
}

.img-center{ 
	margin: 0 auto;
	text-align: center;
}	
	
.table-1 {
	border: 2px solid #8b8f8f;
	position: absolute;
}
.table-2{
    position: relative;
    width: 100%;
}
.table-3 {
    border: 2px solid #3bb779;
    position: absolute;
/*    width: 95%;
    margin: 10px 15px;*/	
}
.table-3 {
    border: 2px solid #3bb779;
    position: absolute;
    /* width: 95%; */
    /* margin: 10px 15px; */
}

.f-12{font-size: 12px;}

.f-14{font-size: 14px;}
.f-16{font-size: 16px;}
.f-18{font-size: 18px;}

.h2-02{font-size: 18px;}
.h2-3{ font-size: 18px;   padding: 10px 10px;color: #fff;text-transform: capitalize;background-color: #91c74d;}
.h2-4{font-size: 16px;font-weight: 700;text-transform: uppercase;margin: 0;padding: 0px;}
.h2-5{font-size: 14px;font-weight: 400;text-transform: capitalize;margin: 0;padding: 0px;}
.h2-6{font-size: 16px;font-weight: 700;text-transform: capitalize;margin:00px;padding: 0px;}
.h2-7{font-size: 12px;font-weight: 400;margin: 0;padding: 0px;line-height: 1.4;}
.h2-8 {font-size: 18px;font-weight: 700;margin: 0;padding: 0px;line-height: 1.4;}
.h2-9 {font-size: 12px;font-weight: 700;margin: 0;padding: 0px 0 0px 0;line-height: 1.4;}

.border-1{border:1px solid #8b8f8f;}
.border-2{border-bottom:2px solid #3bb779;}
.border-3{border:2px solid #91c74d!important;}
.border-4{border:2px solid #565656;}
.border-5{border:2px solid #828282;}
.border-6{border:2px solid #828282;}
.border-7{border:2px solid #959595;}
.border-8{border-bottom:2px solid #828282;}


.ta-1{height: 100px;width: 100%;background-color: #f1f3fe;resize: none;    margin: 5px 0 6px 0;}

.bg-1{background-color: #8b8f8f;}
.bg-2{background-color: #91c74d;}
.bg-3{background-color: #3bb779;}
.bg-4{background-color: #d3d2d5;}
.bg-5{background-color: #eb3469;}
.bg-6{background-color: #95bc57;}

.row-1 {
    padding: 10px 0;
    height: 40px;
    background-color: #d3d2d8;
}
.row-1 {
    padding: 10px 0;
    height: 60px;
    background-color: #d3d2d8;
}

.color-1{color:#fff;}
.color-2{color:#8b8f8f;}
.color-3{color:#000;}
.color-4{color:#171212;}
.color-5{color:#af9a98;}
.color-6{color:#ec3038;}
.color-7{color:#828282;}


input{background-color: #f1f3fe;border:none;outline: none;padding:1px 10px;}
input {background-color: #f1f3fe;border: none;outline: none;padding: 1px 10px;border-bottom: 2px solid #91c74d;}
.input-1{background-color: #f1f3fe;border:none;outline: none;}
.input-2{background-color: #f1f3fe;border:none;outline: none;height: 25px;}
.input-2{background-color: #f1f3fe;border:none;outline: none;}

.btn-1{background-color: #878b8b;
    border: none;
    color: #fff;
    padding: 3px;
}
.btn-1 {
    background-color: #878b8b;
    border: none;
    color: #fff;
    padding: 5px 0 4px 0;
    font-size: 12px;
}

.head-1{text-align: center;padding: 0;margin:0;}
.h2-1{font-size: 14px;margin: 0;padding: 0;letter-spacing: 0.5px ;}

.h-5{height: 5px;}
.h-10{height: 10px;}
.h-20{height: 20px;}
.h-30{height: 30px;}
.h-40{height: 40px;}
.h-50{height: 50px;}
.h-60{height: 60px;}
.h-70{height: 70px;}
.h-80{height: 80px;}
.h-90{height: 80px;}
.h-190{height: 190px!important;max-height: auto;}
.h-190 {
    height: 250px!important;
    max-height: auto;
}
.h-210{height: 210px!important;}
.h-250{min-height: 250px!important;}
.h-60{height: 70px!important;}


.w-15{width: 15%;float: left;}
.w-20{width: 20%;float: left;}
.w-25{width: 25%;float: left;}
.w-30{width: 30%;float: left;}
.w-40{width: 40%;float: left;}
.w-50{width: 50%;float: left;}
.w-60{width: 60%;float: left;}
.w-70{width: 70%;float: left;}
.w-80{width: 80%;float: left;}
.w-100{width: 100%;float: left;}

.f-700{font-weight: 700 !important;}
.f-400{font-weight: 400 !important;}
.f-300{font-weight: 300 !important;}

.nopadding{padding: 0!important;}
.nomargin{margin:0!important;}
.m-0{margin:0!important;}

.margin-5{margin:10px 0 0 0;}

.m-1{margin:1px;}
.m-2{margin:2px;}
.m-3{margin:3px;}
.m-4{margin:4px;}
.m-5{margin:5px;}
.m-10{margin:10px 0;}

.mt-1{margin-top:1px;}
.mt-2{margin-top:2px;}
.mt-3{margin-top:3px;}
.mt-4{margin-top:4px;}
.mt-5{margin-top:5px;}
.mt-10{margin-top:10px;}
.mt-40{margin-top:40px;}
.mt-50{margin-top:50px;}

.mb-1{margin-bottom:1px;}
.mb-2{margin-bottom:2px;}
.mb-3{margin-bottom:3px;}
.mb-4{margin-bottom:4px;}
.mb-5{margin-bottom:5px;}
.mb-10{margin-bottom:10px;}
.mb-40{margin-bottom:40px;}
.mb-50{margin-bottom:50px;}

.margin-01{    margin: 40px 0px 0 0px;}
.margin-01 {
    margin: 11px 0px 0 0px;
}



.p-100{padding: 100px;}
.p-80{padding: 80px;}
.p-60{padding: 60px;}
.p-40{padding: 40px;}
.p-20{padding: 20px;}
.p-10{padding: 10px;}
.p-17{padding: 17px;}
.p-8{padding: 8px;}
.p-7{padding: 7px;}
.p-6{padding: 6px;}
.p-5{padding: 5px;}
.p-4{padding: 4px;}
.p-3{padding: 3px;}
.p-2{padding: 2px;}
.p-1{padding: 1px;}

.pb-100{padding-bottom: 100px;}
.pb-80{padding-bottom: 80px;}
.pb-60{padding-bottom: 60px;}
.pb-40{padding-bottom: 40px;}
.pb-20{padding-bottom: 20px;}
.pb-10{padding-bottom: 10px;}
.pb-5{padding-bottom: 5px;}

.pt-100{padding-top: 100px;}
.pt-80{padding-top: 80px;}
.pt-60{padding-top: 60px;}
.pt-40{padding-top: 40px;}
.pt-20{padding-top: 20px;}
.pt-10{padding-top: 10px;}
.pt-5{padding-top: 5px;}

.plr-10{padding-left: 10px;padding-right: 10px;}
.plr-20{padding-left: 20px;padding-right: 20px;}
.plr-30{padding-left: 30px;padding-right: 30px;}
.plr-40{padding-left: 40px;padding-right: 40px;}

.pr-zero{padding-right: 0px !important;}


.col-md-37 {width: 37.5%;float: left;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;  display: block;}
.col-md-37 {
    width: 36.5%;
    float: left;
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    display: block;
    margin: 2px;
}

.col-md-6.col-49{width:49%;margin:10px 8px 0 0px;}
.col-md-6.col-49 {
    width: 49%;
    margin:10px 0px;
}
.col-md-6.col-49-2{width:49%;margin:10px 0px 0 9px;}
.col-md-6.col-49-2{width:49%;margin:10px 0px 0 9px;}

.container {
    width: 850px;
}



	</style>
</head>
<body>

    <section class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-3" style="width:22%;float:left;position: relative;padding:0 10px;">
                    <img src="pdf-test-image/STC-Assignment-Form.png" class="img-center" alt="">
                </div>
                <div class="col-md-6 col-sm-6" style="width:50%;float:left;position: relative;padding:0;">
                    <h1 class="h2-02 text-center">STC Assignment Form - PV Solar</h1>
                </div>
                <div class="col-md-3 col-sm-3 p-10" style="width:25%;float:left;position: relative;padding:0;">
                    <div class="table-1">
                        <div class="col-md-12 col-sm-12 bg-1">
                            <p class="head-1 color-1 f-700 p-2">OFFICE USE ONLY</p>
                        </div>     

                        <div class="col-md-12 col-sm-12">
                            <div class="w-100 p-5">
                                <span class="w-20 color-2 f-700">REF </span>
                                <input type="text" class="w-80 input-1">    
                            </div>
                        </div>    
                        <div class="col-md-12 col-sm-12">
                            <div class="w-100 p-5">
                                <span class="w-20 color-2 f-700">PVD </span>
                                <input type="text" class="w-80 input-1">    
                            </div>
                        </div>    
                    </div>
                </div>
            </div><!-- row -->

            <div class="row">
                <div class="col-md-9 nopadding" >
                    <div class="col-md-6 col-49 border-3 nopadding margin-5 " style="">
                        <div class="table-2">
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-700">Installation Date:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100">
                                <h2 class="h2-1 color-4 bg-2 f-700 text-center p-8">Owner Details </h2>
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">First Name:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Last Name: </span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Postal Address:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Suburb:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">State:</span>
                                <input type="text" class="w-70" placeholder="VIC">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Postcode:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Home:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Mobile:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Email:</span>
                                <input type="text" class="w-70">    
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-49-2 border-3 nopadding margin-5" style="">
                        <div class="table-2">
                            <div class="w-100 p-5">
                                <span class="w-40 color-3 f-700">STC Deeming Period:</span>
                                <div class="w-60">
                                    <input type="checkbox"><span> 1 Yr</span>
                                    <input type="checkbox"><span> 5 Yr</span>
                                    <input type="checkbox"><span> 13 Yr</span>
                                </div>
                            </div>
                            <div class="w-100">
                                <h2 class="h2-1 color-4 bg-2 f-700 text-center p-8">Installation Details</h2>
                            </div>
                            <div class="w-100 p-5">
                                <input type="checkbox"><span>  Same as Owner Details </span>
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">First Name:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Last Name: </span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Install Address: </span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-100 p-5">
                                <span class="w-30 color-3 f-400">Suburb:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">State:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Postcode:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Home:</span>
                                <input type="text" class="w-70">    
                            </div>
                            <div class="w-50 p-5">
                                <span class="w-30 color-3 f-400">Mobile:</span>
                                <input type="text" class="w-70">    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 nopadding" style="">
                    <div class="table-3">
                      
                        <div class="w-100">
                            <h2 class="h2-1 color-1 bg-3 f-700 text-center p-8">Solar Panel System </h2>
                        </div>
                       
                        <div class="w-100 p-5 border-2">
                            <span class="w-100 color-5 f-400 text-right">Panel Brand</span>
                            <input type="text" class="w-100 input-2" placeholder="LG NEON2 330W">     
                        </div>

                        <div class="w-100 p-5 border-2">
                            <span class="w-100 color-5 f-400 text-right">Panel Model</span>
                            <input type="text" class="w-100 input-2" placeholder="LG330N1C(W)-G4">    
                        </div>

                        <div class="w-100 p-5 border-2">
                            <span class="w-100 color-5 f-400 text-right">Inverter Brand </span>
                            <input type="text" class="w-100 input-2" placeholder="Enphase Energy">    
                        </div>

                        <div class="w-100 p-5 border-2">
                            <span class="w-100 color-5 f-400 text-right">Inverter Model</span>
                            <input type="text" class="w-100 input-2" placeholder="Micro Inverters">    
                        </div>

                        <div class="w-100 p-5 border-2-1">
                            <span class="w-100 color-5 f-400 text-right">Inverter Series</span>
                            <input type="text" class="w-100 input-2" placeholder="S270-72-LN-2AU">    
                        </div>

                    </div>
                
                </div>

                <div class="col-md-9 nopadding margin-5">
                    <div class="col-md-3 border-4 h-250">
                        <p class="text-center p-5">Are you replacing panels to a system as a result of damage or faults?</p>
                        <div class="text-center">
                            <div class="w-100 p-5">
                              <input type="checkbox">  <span class="p-5"> Yes</span>
                                <input type="checkbox"><span class="p-5"> No</span>
                            </div>    
                        </div>
                        <p class="text-center m-10 h-60"># of replacement panels? </p>
                        <div class="w-100 ">
                            <input type="text" class="w-100 input-all border-5 m-10">    
                        </div>
                    </div>    
                    <div class="col-md-3 border-4 h-250">
                        <p class="text-center p-5">Are you installing additional panels to an existing system? </p>
                        <div class="text-center">
                            <div class="w-100 p-5">
                                <input type="checkbox">  <span class="p-5"> Yes</span>
                                <input type="checkbox"><span class="p-5"> No</span>
                            </div>    
                        </div>
                        <p class="text-center m-10 h-60"># of existing panels?  </p>
                        <div class="w-100 ">
                            <input type="text" class="w-100 input-all border-5 m-10">    
                        </div>
                    </div>    
                    
                    <div class="col-md-3 border-4 h-250">
                        <p class="text-center p-5">Is there currently more than one system installed at this address? </p>
                        <div class="text-center">
                            <div class="w-100 p-5">
                                <input type="checkbox">  <span class="p-5"> Yes</span>
                                <input type="checkbox"><span class="p-5"> No</span>
                            </div>    
                        </div>
                        <p class="text-center m-10 h-60">please specify location of other system? </p>
                        <div class="w-100 ">
                            <input type="text" class="w-100 input-all border-5 m-10">    
                        </div>
                    </div>    

                    <div class="col-md-3 border-4 h-250">
                        <p class="text-center p-5">Are there any additional comments relating to this installation? </p>
                        
                        <div class="w-100 ">
                            <textarea name="" id="" rows="5" class="ta-1 border-5"></textarea>
                        </div>
                    </div>    
                    
                </div>

                <div class="col-md-3 pr-zero pb-5">
                    <div class="border-6 w-100 bg-4 margin-01 p-17">
                        <p class="m-0 text-center">Number of Panels</p>
                        <input type="text" class="w-100 border-4 m-10">    
                        <p class="m-0 text-center">Rated Power Output (kW) </p>
                        <input type="text" class="w-100 border-4 m-10">    
                    </div>
                </div>

                <div class="col-md-12 nopadding mt-10">
                    <div class="row-1 bg-5">
                        <div class="col-md-8">
                            <span class="w-20 color-6 f-700">Property Type:</span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> Residential</span></span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> School</span></span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> Commercial</span></span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> Other</span></span>
                        </div>
                        <div class="col-md-4">
                            <span class="w-100"><input type="text" class="w-100 border-6" /></span>
                        </div>
                    </div>
                    <div class="row-1 bg-5">
                        <div class="col-md-8">
                            <span class="w-20 color-6 f-700">Single/Multi Story:</span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> Single</span></span>
                            <span class="w-20"><input type="checkbox">  <span class="p-5"> Multi</span></span>
                            <span class="w-40"><input type="checkbox">  <span class="p-5">  Number of small-scale tech certs (STCs) </span></span>
                            
                        </div>
                        <div class="col-md-4">
                            <span class="w-100"><button class="w-40 btn-1">Number of STCs</button><input type="text" class="w-60 border-6" /></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 nopadding mt-5">
                    <h2 class="h2-3">
                        Accreditation Information
                    </h2>

                    <h3 class="h2-4 col-md-12">
                        INSTALLER DETAILS 
                    </h3>
                    <div class="col-md-12 nopadding mt-5 color-7">
                        <div class="col-md-3 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">FULL NAME </span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" placeholder="03 9770 8780"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">PHONE</span>
                            </div>
                        </div>
                        <div class="col-md-5 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" placeholder="PO Box 153 Braeside 3195"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-4 nopadding text-left f-12">ADDRESS </span>
                                <span class="col-md-4 nopadding text-center f-12">SUBURB</span>
                                <span class="col-md-4 nopadding text-right f-12">POSTCODE</span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-12 nopadding text-center f-12">ACCREDITATION NUMBER </span>
                            </div>
                        </div>
    
                    </div>

                    <h3 class="h2-4 mt-10 col-md-6 text-left">
                        ELECTRICIAN DETAILS
                    </h3>
                    <h3 class="h2-5 mt-10 col-md-6 text-right">
                        • State 'as above' if details are the same 
                    </h3>
                    <div class="col-md-12 nopadding mt-5 color-7">
                        <div class="col-md-3 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" placeholder="AS ABOVE"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">FULL NAME </span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" />
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">PHONE</span>
                            </div>
                        </div>
                        <div class="col-md-5 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-4 nopadding text-left f-12">ADDRESS </span>
                                <span class="col-md-4 nopadding text-center f-12">SUBURB</span>
                                <span class="col-md-4 nopadding text-right f-12">POSTCODE</span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-12 nopadding text-center f-12">ACCREDITATION NUMBER </span>
                            </div>
                        </div>
    
                    </div>

                    <h3 class="h2-4 mt-10 col-md-6 text-left">
                        DESIGNER DETAILS
                    </h3>
                    <h3 class="h2-5 mt-10 col-md-6 text-right">
                        • State 'as above' if details are the same 
                    </h3>
                    <div class="col-md-12 nopadding mt-5 color-7">
                        <div class="col-md-3 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" placeholder="AS ABOVE" />
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">FULL NAME </span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" />
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">PHONE</span>
                            </div>
                        </div>
                        <div class="col-md-5 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6" />
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-4 nopadding text-left f-12">ADDRESS </span>
                                <span class="col-md-4 nopadding text-center f-12">SUBURB</span>
                                <span class="col-md-4 nopadding text-right f-12">POSTCODE</span>
                            </div>
                        </div>
                        <div class="col-md-2 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-6"/>
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="col-md-12 nopadding text-center f-12">ACCREDITATION NUMBER </span>
                            </div>
                        </div>
    
                    </div>

                </div>

                <div class="col-md-12 nopadding mt-10 mb-10 h-5 bg-6">
                    
                </div>

                <div class="col-md-12 nopadding mt-5 color-3">
                    
                    <p class="color-3 f-700 f-12"> Mandatory written statement by the cec Installer and designer: </p>
                    <p class="color-3 f-700 f-12">
                        <span>I</span> <input type="text" class="w-10 border-8" /> <span> (name of Installer) was the accredited CEC Installer that completed the SGU installation at</span>
                        <input type="text" class="w-10 border-8" /> and verify that I have Installed the system, It meets
                        the CEC accreditation guidelines, CEC Accreditation Code of Practice and I am bound by their Code of Conduct, have used panels and inverters approved by the CEC, followed all of the Clean Energy Regulator's Guidelines, have $5m in Public
                        Liability Insurance and the system meets the following Australian Standards, where applicable:-
                    </p>

                </div>   
                
                <div class="col-md-12 nopadding margin-5 mb-10 color-4">
                    <div class="col-md-4 border-7 pt-10 h-190">
                        <h2 class="h2-6 pt-2">PV & Inverter Standards</h2>
                        <h2 class="h2-7 pt-5 f-700">AS/NZS 5033:2005, Installation of photovoltaic (PV) arrays </h2>
                        <h2 class="h2-7 pt-2 f-700">AS/NCS 1170:2002, Structural Design actions, Part 2: Wind Action (PV Array)  </h2>
                        <h2 class="h2-7 pt-2 f-700">AS/NZS 5033, PV modules are compliant and the product is listed at standard AS 4086:2:1997, Secondary batteries for use with standalone www.cleanenergycouncil.org.au  </h2>
                        <h2 class="h2-7 pt-2 pb-5 f-700">The grid connected inverter used has bean tasted to Standard AS 47TT and the product is listed at cleananargycouncil.org.au </h2>
                    </div>    

                    <div class="col-md-4 border-7 pt-10 h-190">
                        <h2 class="h2-6 pt-2">Grid connected system </h2>
                        <h2 class="h2-7 pt-5 f-700">AS/NZS 3000:2007, Wiring Rules  </h2>
                        <h2 class="h2-7 pt-2 f-700">AS 4777, this installation complies to this standard  </h2>
                        <h2 class="h2-7 pt-2 f-700">AS/NZS 51768:2007, Lightning Protection </h2>
                        <h2 class="h2-7 pt-2 pb-5 f-700">AS 4777:2005, Grid connection of energy system via Inverters  </h2>
                    </div>    

                    <div class="col-md-4 border-7 pt-10 h-190">
                        <h2 class="h2-6 pt-2">Standalone System </h2>
                        <h2 class="h2-7 pt-5 f-700">AS/NZS 4509:2009, Standalone Power systems part 1: Safety & Installation </h2>
                        <h2 class="h2-7 pt-2 f-700">AS 4086:2:1997, Secondary batteries for use with standalone power system, Part 2: Installation & maintenance, wind system  </h2>
                        <h2 class="h2-7 pt-2 f-700">AS/NZS 3000:2007, Wiring Rules  </h2>
                    </div>    

                </div>

                <div class="col-md-12 nopadding margin-5 mb-10 color-4">
                   <span class="f-700">I verify that all local, State or Territory government requirements have been met for. (i) The siting of the unit (ii) The attachment of the unit to the building or structure. (iii) The grid connection of the system
                        for the SGU installation</span><span>I verify that the SGU is </span>
                        <span class="plr-10"><input type="checkbox">  <span class="p-5"> Grid connected </span>
                        <span class="plr-10"><input type="checkbox">  <span class="p-5">  an Off grid installation and an electrical worker holding an unrestricted licence for electrical work issued by the State or Territory authority for the
                                place where the unit was installed undertook all wiring of the unit that involves alternating current of 50 or more volts or direct current of 120. I confirm that the details in the above statement is correct.  </span>
                </div>           

                
                <div class="col-md-6 mt-5 color-7">
                    <div class="col-md-9 nopadding ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding">
                            <span class="f-12">Signature of the SGUs CEC Installer </span>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding text-center">
                            <span class="f-12 ">CEC Number</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mt-5 color-7">
                    <div class="col-md-9 nopadding ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding">
                            <span class="f-12">Signature of the SGUs CEC Designer </span>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding text-center">
                            <span class="f-12 ">CEC Number</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mt-5 color-7">
                    <div class="col-md-9 nopadding ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding">
                            <span class="f-12">Print Name </span>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding text-center">
                            <span class="f-12 ">Date</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mt-5 color-7">
                    <div class="col-md-9 nopadding ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding">
                            <span class="f-12">Print Name </span>
                        </div>
                    </div>
                    <div class="col-md-3  ">
                        <div class="col-md-12 nopadding">
                            <input type="text" class="w-100 border-8">
                        </div>    
                        <div class="col-md-12 nopadding text-center">
                            <span class="f-12 ">Date</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mt-5 color-4 nopadding">
                    <div class="col-md-12 border-7 pt-10 pb-10 mb-10 mt-10">
                        <h2 class="h2-8">Mandatory Declaration</h2>
                        <h2 class="h2-9 pb-10 pt-10">I am the legal owner of the above small generation unit (SGU) and assign the right to create STCs to KB
                                Quality Airconditioning Pty Ltd t/as Green Sky Australia for the period stated above, commencing at the
                                date of installation. I have not preiously assigned or created any STCs for this system within this period
                                To claim 14 years deeming for SGU. STCs must be registered within 12 months or installation
                                I understand I am under no obligation to assign STCs to Green Sky Australia. I agree to repay the STC to
                                Green Sky Australia should my assignment be invalid.I understand that an agent of the Clean Energy
                                Regulator or Green Sky Australia may wish to inspect the SGU within the five years of certificate
                                redemption. I must retain receipts and proof of the installation date for the life of the STCs. I am aware that
                                penalties can be applied for providing misleading information in the form under the Renewable energy
                                (Electricity) Act 2000 
                        </h2>
                        <h2 class="h2-9 pb-10">
                            I further declare that the accredited CEC Installer named on this form physically attended the installation of the unit    
                        </h2>

                    </div>                    
                </div>

                <div class="col-md-6 mt-5 color-4">
                    <div class="col-md-12 nopadding mt-5 color-3 mb-10 mt-10">
                        <p class="color-3 f-700 f-12">
                            <span>I understand that this system is eligible for </span> <input type="text" class="w-10 border-8" />
                            <span> STCs and in exchange for assigning my
                                    right to create these STCs, I will receive a point of sale discount from the installers/suppliers. </span>
                        </p>
                    </div> 
                    <div class="col-md-6 mt-5 color-7 mt-40 nopadding">
                        <div class="col-md-7 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-8">
                            </div>    
                            <div class="col-md-12 nopadding ">
                                <span class="f-12">Owner Signature</span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-8">
                            </div>    
                            <div class="col-md-12 nopadding text-center">
                                <span class="f-12 ">Date </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 mt-5 color-7 mt-40 nopadding">
                        <div class="col-md-7 nopadding ">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-8">
                            </div>    
                            <div class="col-md-12 nopadding">
                                <span class="f-12">Agent/Installer Signature </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-12 nopadding">
                                <input type="text" class="w-100 border-8">
                            </div>    
                            <div class="col-md-12 nopadding text-center">
                                <span class="f-12 ">Date </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-5 color-4 nopadding">
                    <p><span class="f-700">PRIVACY DECLARATION:</span><span class="f-14"> Green Sky Australia will only use this personal information as intended and will not sell
                            or divulge this to any third parties other than the Clean Energy Regulators.</span> </p>
                    </div>
                </div>

            </div>
        </div><!-- end of container -->
    </section><!-- end of main-content section -->

</body>
</html>