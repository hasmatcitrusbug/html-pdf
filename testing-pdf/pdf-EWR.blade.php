<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EWR Form</title>
    <style>
             
    </style>
</head>
<body style="font-family:'Arial Rounded MT Bold';">
   
    <table style="background:#fff;width:800px;padding:30px 20px;">
        <tr>
           <td>
                <table style="padding:0 0 20px 0;">
                    <tr>
                        <td style="width:300px;margin:0;padding:10px;vertical-align:top;border:1px solid #000;">
                            <table>
                                <tr>
                                    <td style="width:150px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        Retailer:
                                    </td>
                                    <td style="width:150px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        <input type="text" style="width:150px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        Service Order:#
                                    </td>
                                    <td style="width:150px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        <input type="text" style="width:150px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        NMI:
                                    </td>
                                    <td style="width:150px;font-size:16px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        <input type="text" style="width:150px;">
                                    </td>
                                </tr>
                                
                            </table>
                        </td> 
                        <td style="width:400px;margin:0;vertical-align:bottom;"><img class="logo" src="http://18.191.53.95/dev/greensky/public/assets/images/ewr.png" style="width:400px;"></td> 
                    </tr>
                </table>
            <td>
        </tr>

        <!-- ************************ -->

        <tr>
           <td>
                <table style="padding:0 0 20px 0;">
                    <tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
                        <td style="width:800px;margin:0;padding:10px;vertical-align:top;">
            
                                <tr>
                                    <td style="width:800px;text-align:left;font-size:16px;color: #003f87;font-weight: 700;padding:0 0 0px 0;">
                                        <b>Application for service – Electrical Works Request (EWR) for new and existing installations</b>
                                    </td>
                                </tr>
                            </table>
                        </td>    
                    </tr>

                    <tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
                        <td style="width:800px;margin:0;padding:10px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="width:300px;font: 12px 'Arial Narrow';color: #c00000;padding:10px 0 10px 0;">
                                        Send completed form by email or mail to Retailer:
                                    </td>
                                    <td style="width:100px;font-size:12px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        <input type="text" style="width:100px;">
                                    </td>
                                    <td style="width:300px;font: 12px 'Arial Narrow';color: #c00000;line-height: 16px;padding:10px 0 10px 10px;">
                                        Send completed form by email or mail to Retailer:
                                    </td>
                                    <td style="width:100px;font-size:12px;padding:0 0 0px 0px;color:#000;line-height:30px;text-align:left;">
                                        <input type="text" style="width:100px;">
                                    </td>
                                </tr>
                            </table>
                        </td>    
                    </tr>

                    <tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
                        <td style="width:800px;margin:0;padding:10px 10px 0px 10px;font: 12px 'Arial';">
                            I acknowledge the requirements of the relevant Distribution Company have been adhered to and certify the electrical work complies with the Victorian Service & Installation Rules and the Electricity Safety Act and Regulations. I also acknowledge the initial connection will not be connected without a Prescribed Certificate of Electrical Safety and that I am responsible for any associated Distribution Company charges unless the Retailer has accepted all charges.
                        </td>    
                    </tr>

                    <tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
                        <td style="width:800px;margin:0;padding:10px 0 0 10px;font: bold 14px 'Arial';color: #003f87;">
                            Work site address
                        </td>    
                    </tr>

                    <tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
                        <td style="width:800px;margin:0;padding:0px 0 0 10px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="width:200px;font: 12px 'Arial Narrow';color: #003f87;padding:10px 0 10px 0;">
                                        Customer or business name
                                    </td>
                                    <td style="width:200px;font-size:12px;padding:0 0 0px 0px;color:#000;text-align:left;">
                                        <input type="text" style="width:200px;">
                                    </td>
                                    <td style="width:100px;font: 12px 'Arial Narrow';color: #003f87;padding:10px 0 10px 20px;">
                                        Ph # / Mb #
                                    </td>
                                    <td style="width:300px;font-size:12px;padding:0 0 0px 0px;color:#000;text-align:left;">
                                        <input type="text" style="width:300px;">
                                    </td>
                                </tr>
                            </table>
                        </td>    
                    </tr>
					
					<tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
						<td style="width:800px;margin:0;padding:0px 0 0 10px;vertical-align:top;color:#003f87!important;">
							<table>
								<tr>
									<td style="width:120px;margin:0;">
										<b>Site Type:</b>
									</td>
									<td style="width:20px;margin:0;">
										<input type="checkbox">
									</td>
									<td style="width:100px;margin:0;">
										Factory 
									</td>
									<td style="width:20px;margin:0;">
										<input type="checkbox">
									</td>
									<td style="width:100px;margin:0;">
										Suite
									</td>
									<td style="width:20px;margin:0;">
										<input type="checkbox">
									</td>
									<td style="width:100px;margin:0;">
										Tenancy 
									</td>
									<td style="width:20px;margin:0;">
										<input type="checkbox">
									</td>
									<td style="width:100px;margin:0;">
										Shop  
									</td>
									<td style="width:50px;margin:0;">
										Other
									</td>
									<td style="width:160px;margin:0;">
										<input type="text" style="width:160px;border: 1px solid #828282;" placeholder="Residential/home">
									</td>    
								</tr>
							</table>  
						</td>
					</tr>	
					
					<tr style="padding:0 0 5px 0!important;margin:0 0 0 0!important;">
						<td style="width:800px;margin:0;padding:0px 0 0 10px;vertical-align:top;color:#003f87!important;">
							<table>
								<tr>
									<td colspan="6" style="width:800px;text-align:left;margin:0;color: #000;">
										<b>INSTALLER DETAILS</b>
									</td>
								</tr>
								<tr>
									<td style="width:149px;margin:0;color: #000;">
										<input type="text" style="width:149px;border:2px solid #828282;">
									</td>
									<td style="width:149px;margin:0;color: #000;">
										<input type="text" style="width:149px;border:2px solid #828282;">
									</td>
									<td colspan="3" style="width:349px;margin:0;color: #000;">
										<input type="text" style="width:339px;border:2px solid #828282;">
									</td>
									<td style="width:149px;margin:0;color: #000;">
										<input type="text" style="width:149px;border:2px solid #828282;">
									</td>
								</tr>
								
							</table>    
						</td>
					</tr>		
					
					
                                    


                </table>
            <td>
        </tr>



    </table>

</body>
</html>

		