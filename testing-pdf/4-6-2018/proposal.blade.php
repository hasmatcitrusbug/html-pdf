<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proposal Page</title>
</head>
<body>
    <table style="background:#d9d9d9;width:810px;padding:0px 25px;page-break-after: always;">
        <tr>
           <td>
                <table style="padding:30px 0 30px 0;border-bottom:5px solid #0000cc;">
                    <tr>
                        <td style="width:250px;margin:0;vertical-align:bottom;"><img class="logo" src="http://18.191.53.95/dev/greensky/public/assets/images/greensky_logo.png" style="width:200px;"></td> 
                        <td style="width:250px;margin:0;padding:0 0 0 20px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:12px;padding:0 0 25px 0;">Address</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">3 / 2 Interchange Way</td>
                                </tr>    
                                <tr>
                                    <td style="font-size:10px;padding:0 0 0px 0;">Carrum Downs, Vic, 3201</td>
                                </tr>
                            </table>
                        </td> 
                        <td style="width:250px;margin:0;">
                            <table>
                                <tr>
                                    <td style="font-size:12px;padding:0 0 25px 0;vertical-align:top;">Contact</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:80px;">Phone:</td>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:150px;">(03) 9770 8780</td>
                                </tr>    
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:80px;">E-Mail:</td>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:150px;">info@greenskyaustralia.com.au</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:80px;">Web:</td>
                                    <td style="font-size:10px;padding:0 0 5px 0;width:150px;">www.greenskyaustralia.com.au</td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table><!-- end top page - 1  -->
                
                <table style="padding:250px 0 320px 0;">
                    <tr>
                        <td style="width:750px;margin:0;padding:0;vertical-align:middle;font-size:72px;color:#454547;text-align:left;font-weight:900;">
                            Proposal
                        </td> 
                    </tr>
                    <tr>
                        <td style="width:750px;margin:0;padding:0 0 0 5px;vertical-align:middle;font-size:22px;color:#454547;text-align:left;font-weight:700;">
                            Make your dream come true
                        </td> 
                    </tr>
                </table><!-- middle content - 2 -->

                <table style="padding:20px 0 40px 0;margin:60px 0 42px 0;background:#8cc540;">
                    <tr>
                        <td style="width:600px;margin:0;padding:0 0 0 20px;">
                            <table>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Prepared for:</td>
                                </tr>    
                                <tr>
                                    <td style="font-size:12px;padding:0 0 5px 0;">John Peterson</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Address: Bul Tenesse Hord / 12/ 3/55</td>
                                </tr>    
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Tel: 00378 565 5666 666</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Fax:00378 656 9685 641</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">E-mail: lendesia@lendesia.com</td>
                                </tr>
                                
                            </table>
                        </td> 
                        <td style="width:150px;margin:0;">
                            <table>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Proposal Issued:</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">15 July 2016</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">Proposal Validation:</td>
                                </tr>
                                <tr>
                                    <td style="font-size:10px;padding:0 0 5px 0;">15 August 2016</td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table><!-- end top page - 1  -->
            <td>
        </tr>
    </table>
   
    <!-- <p style="">&nbsp;</p> -->
   
    <table style="background:#fff;width:850px;padding:30px 20px;">
        <tr>
           <td>
                <table style="padding:100px 0 40px 0;">
                    <tr>
                        <td style="width:750px;margin:0;">
                            <table>
                                <tr>
                                    <td style="font-size:27px;padding:0 0 10px 30px;color:#0000cc;font-weight:700;"><b>STATEMENT</b></td>
                                </tr>
                                <tr>
                                    <td style="font-size:27px;padding:0 0 25px 30px;color:#0000cc;font-weight:700;"><b>OF OUR PRESIDENT</b></td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table><!-- end top page - 2  -->
                
                <table style="padding:0 0 20px 0;">
                    <tr>
                        <td style="width:280px;margin:0;padding:0 20px 0 20px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:16px;padding:0 0 25px 10px;color:#8cc540;line-height:30px;text-align:left;">Thank you for your interest in our Solar Power systems. Creating your own electricity from a renewable source is not only good for our environment, it is also a great investment and like any investment it should be considered carefully before you commit your hard-earned dollars.</td>
                                </tr>
                                
                            </table>
                        </td> 
                        <td style="width:450px;margin:0;vertical-align:bottom;"><img class="logo" src="http://18.191.53.95/dev/greensky/public/assets/images/img-2222.png" style="width:450px;"></td> 
                        
                    </tr>
                </table><!-- end top page - 1  -->            

                <table style="padding:0 0 20px 0;">
                    <tr>
                        <td style="width:250px;margin:0;padding:0 0 0 20px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">Green Sky Australia is a well-established family owned and operated business. At Green Sky Australia we pride ourselves on the provision of quality systems and services to you, our valued customer. Our Installation teams are our respected employees, highly-qualified and committed to providing quality service.</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">Green Sky Australia are a trusted trade partner of Q CELLS Australia, as we prefer to install only the best products. We are now seeing some of the cheaper products installed as little as 5 years ago, start to fail. We recommend choosing high-quality products to protect your solar investment.</td>
                                </tr>
                            </table>
                        </td> 
                        <td style="width:250px;margin:0;padding:0 0 0 20px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">Solar energy is clean, renewable and free. In Australia we live in the perfect climate for solar power, lots of sun and not much rain. The conversion from sunlight to power is direct and effective, resulting in much-reduced power bills.</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">Renewable energy is a sustainable resource which means that we can avoid the depletion of natural resources for future generations. Not only this but the energy is clean as we do not produce carbon dioxide, there is no water consumption, thermal pollution, waste, noise or adverse land-use impacts.</td>
                                </tr>
                            </table>
                        </td> 
                        <td style="width:250px;margin:0;padding:0 0 0 20px;vertical-align:top;">
                            <table>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">Installing a 1kW grid-connected system is a great step toward helping our environment and is the equivalent of planting approximately 93 trees or reducing your greenhouse gas emissions by approximately 1.86 tonnes each year.</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;padding:0 0 25px 10px;line-height:20px;text-align:justify;">The solar industry now has many case studies in place to prove it is an excellent investment and great for our environment. It’s no longer a matter of having to prove its value, but more a matter of choosing the right company for your needs. Take the stress out of buying Solar power and speak with a company that you can trust.</td>
                                </tr>

                            </table>
                        </td> 
                    </tr>
                </table><!-- end top page - 1  -->            

                

            <td>
        </tr>
    </table>

   

</body>
</html>

